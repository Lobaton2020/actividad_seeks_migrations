<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            ["status" => "Activo"],
            ["status" => "Inactivo"],
        ];
        foreach ($statuses as $value) {
            $status = new Status;
            $status->status = $value["status"];
            $status->save();
        }
    }
}
