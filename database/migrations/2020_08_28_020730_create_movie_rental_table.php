<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieRentalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_rental', function (Blueprint $table) {
            $table->bigIncrements("movie_rental_id");
            $table->bigInteger("category_id")->unsigned();
            $table->bigInteger("movie_id")->unsigned();
            $table->bigInteger("rental_id")->unsigned();
            $table->integer("price");
            $table->text("observations")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_rental');
    }
}
