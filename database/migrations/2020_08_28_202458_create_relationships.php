<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statuses', function (Blueprint $table) {
            $table->foreign("type_status_id")->references("type_status_id")->on("type_statuses")->onUpdate("cascade");
        });

        Schema::table('rols', function (Blueprint $table) {
            $table->foreign("status_id")->references("status_id")->on("statuses")->onUpdate("cascade");
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign("status_id")->references("status_id")->on("statuses")->onUpdate("cascade")->change();
            $table->foreign("rol_id")->references("rol_id")->on("rols")->onUpdate("cascade");
        });

        Schema::table('movies', function (Blueprint $table) {
            $table->foreign("user_id")->references("user_id")->on("users")->onUpdate("cascade");
            $table->foreign("status_id")->references("status_id")->on("statuses")->onUpdate("cascade");
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign("status_id")->references("status_id")->on("statuses")->onUpdate("cascade");
        });

        Schema::table('category_movies', function (Blueprint $table) {
            $table->foreign("movie_id")->references("movie_id")->on("movies")->onUpdate("cascade");
            $table->foreign("category_id")->references("category_id")->on("categories")->onUpdate("cascade");
        });

        Schema::table('rentals', function (Blueprint $table) {
            $table->foreign("user_id")->references("user_id")->on("users")->onUpdate("cascade");
            $table->foreign("status_id")->references("status_id")->on("statuses")->onUpdate("cascade");
        });

        Schema::table('movie_rental', function (Blueprint $table) {
            $table->foreign("category_id")->references("category_id")->on("categories")->onUpdate("cascade");
            $table->foreign("movie_id")->references("movie_id")->on("movies")->onUpdate("cascade");
            $table->foreign("rental_id")->references("rental_id")->on("rentals")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relationships');
    }
}
